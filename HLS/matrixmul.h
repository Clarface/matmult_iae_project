//*****************************************************************************
// Based on a original example from Xilinx
//
// Modified by G.Sutter for teaching purposes
// Revision History:
//  - march 2012, for AutoESL 2011.4.2
//  - dic 2012, for Vivado HLS 2012.3
//  - march 2014, for Vivado HLS 2013.3
//
//*****************************************************************************
#ifndef __MATRIXMUL_H__
#define __MATRIXMUL_H__

#include <cmath>
#include <ap_axi_sdata.h>
#include <hls_stream.h>
#include <ap_fixed.h>

using namespace std;

#define MY_TYPE float //Solution v_float
//#define MY_TYPE ap_fixed<32,8> //Solution 32,6
//#define MY_TYPE ap_fixed<20,6> //Solution 20,6
//#define MY_TYPE ap_fixed<16,6> //Solution 16,6


//#define MAT_A_ROWS 32
//#define MAT_A_COLS 32
//#define MAT_B_ROWS 32
//#define MAT_B_COLS 32
#define MAT_DIM 32

typedef ap_uint<32> UINT_VALUE;

typedef MY_TYPE mat_a_t;
typedef MY_TYPE mat_b_t;
typedef MY_TYPE result_t;

typedef ap_axiu<32,4,5,5> AXI_VALUE;

// Prototype of top level function for C-synthesis

void matrixtrans(
	mat_b_t b[MAT_DIM][MAT_DIM],
	result_t res[MAT_DIM][MAT_DIM]);

void matrixinv(
	mat_a_t a[MAT_DIM][MAT_DIM],
	result_t res[MAT_DIM][MAT_DIM]);

void matrixmul(
	mat_a_t a[MAT_DIM][MAT_DIM],
	mat_b_t b[MAT_DIM][MAT_DIM],
	result_t res[MAT_DIM][MAT_DIM]);

void matrixmul_accel_core (
	UINT_VALUE input[2 * MAT_DIM * MAT_DIM],
	UINT_VALUE output[MAT_DIM * MAT_DIM]);

#endif // __MATRIXMUL_H__ not defined

