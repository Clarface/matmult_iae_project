//*****************************************************************************
// A simple testbench to simulate
// matrix multiplication of floating points and the AXI-Stream core.
//
// by G.Sutter for teaching purposes
// Revision History:
//  - march 2014, for Vivado HLS 2013.3
//
//*****************************************************************************
#include <iostream>
#include "matrixmul.h"

void gen_matrix_ab(mat_a_t a[MAT_DIM][MAT_DIM],mat_b_t b[MAT_DIM][MAT_DIM]);
void gen_sw_matmult(mat_a_t a[MAT_DIM][MAT_DIM], mat_b_t b[MAT_DIM][MAT_DIM],result_t sw_res[MAT_DIM][MAT_DIM]);
void check_matmult_result( result_t hw_res[MAT_DIM][MAT_DIM], result_t sw_res[MAT_DIM][MAT_DIM], int &err_cnt);

using namespace std;


int test_matrix_mul()
{ //for basic matrix multiplication
   mat_a_t in_mat_a[MAT_DIM][MAT_DIM];
   mat_b_t in_mat_b[MAT_DIM][MAT_DIM];
   mat_a_t in_mat_a_inv[MAT_DIM][MAT_DIM];
   mat_b_t in_mat_b_trans[MAT_DIM][MAT_DIM];
   result_t hw_result[MAT_DIM][MAT_DIM], sw_result[MAT_DIM][MAT_DIM];
   int err_cnt = 0;

   //----------------------------------------------------
   gen_matrix_ab(in_mat_a, in_mat_b);

   matrixtrans(in_mat_b , in_mat_b_trans);
   matrixinv(in_mat_a , in_mat_a_inv);

   gen_sw_matmult(in_mat_a_inv, in_mat_b_trans, sw_result);

   // Run the Vivado-HLS matrix multiply block
   matrixmul(in_mat_a_inv, in_mat_b_trans, hw_result);


   cout << "Result by HW" << endl;
   check_matmult_result(sw_result, hw_result, err_cnt);

   if (err_cnt)
      cout << "ERROR: " << err_cnt << " mismatches detected!" << endl;
   else
      cout << "Test passed. No errors" << endl;

   return err_cnt;
}

float sign(float in){return (in < 0 ? -1 : 1);}

void compute_matmult_mean_err(
    result_t hw_res[MAT_DIM][MAT_DIM],
    float *mean_err)
{
	float accum;

	float tmp;
	for (int i = 0; i < MAT_DIM; i++) {
	   for (int j = 0; j < MAT_DIM; j++) {
			accum = 0;
			Prod: for(int k = 0; k < MAT_DIM; k++) {
			   accum += ((float)(i + k) / (2 * MAT_DIM)) * ((float)(k * j) / (MAT_DIM * MAT_DIM));
			}
			tmp = accum - (float)hw_res[i][j];
			*mean_err += sign(tmp) * tmp;
	   }
	}
	*mean_err = *mean_err / (MAT_DIM * MAT_DIM);
}


//----------------------------------------------------
// Test the streaming interface.
int test_matrix_mul_core()
{
   //for basic matrix multiplication
   mat_a_t in_mat_a[MAT_DIM][MAT_DIM];
   mat_b_t in_mat_b[MAT_DIM][MAT_DIM];
   mat_a_t in_mat_a_inv[MAT_DIM][MAT_DIM];
   mat_b_t in_mat_b_trans[MAT_DIM][MAT_DIM];
   result_t hw_result[MAT_DIM][MAT_DIM], sw_result[MAT_DIM][MAT_DIM];

   UINT_VALUE input [MAT_DIM*MAT_DIM*2];
   UINT_VALUE output [MAT_DIM * MAT_DIM];
   int i , j , err_cnt = 0;
   float mean_err=0;



   //----------------------------------------------------
   gen_matrix_ab(in_mat_a, in_mat_b);

   matrixtrans(in_mat_b , in_mat_b_trans);
   matrixinv(in_mat_a , in_mat_a_inv);

   gen_sw_matmult(in_mat_a_inv, in_mat_b_trans, sw_result);


   //convert matrix in input stream
	for(i = 0; i < MAT_DIM; i++) {
		for(j = 0; j < MAT_DIM; j++) {
			union {	unsigned int oval; float ival; } converter;
			converter.ival = in_mat_a_inv[i][j];
			input[i * MAT_DIM + j] = converter.oval;
		}
	}

	for(i = 0; i < MAT_DIM; i++) {
		for(j = 0; j < MAT_DIM; j++) {
			union {	unsigned int oval; float ival; } converter;
			converter.ival = in_mat_b_trans[i][j];
			input[MAT_DIM * MAT_DIM + i * MAT_DIM + j] = converter.oval;
		}
	}

   // Run the Vivado-HLS matrix multiply block
   matrixmul_accel_core(input, output);

   //convert  output stream in matrix
	for(i = 0; i < MAT_DIM; i++) {
		for(j = 0; j < MAT_DIM; j++) {
			union {	unsigned int ival; float oval; } converter;
			converter.ival = output[i * MAT_DIM + j];
			hw_result[i][j] = converter.oval;
		  //hw_result[i][j] = aValue.data ;
		}
	}

   cout << "Result by HW" << endl;
   check_matmult_result(hw_result, sw_result, err_cnt);

   compute_matmult_mean_err(hw_result, &mean_err);

   cout << "Mean error between float and fixed point: " << mean_err << endl;

   if (err_cnt)
      cout << "ERROR: " << err_cnt << " mismatches detected!" << endl;
   else
      cout << "Test passed. No errors" << endl;

   return err_cnt;
}

//----------------------------------------------------
void gen_matrix_ab(
    mat_a_t a[MAT_DIM][MAT_DIM],
    mat_b_t b[MAT_DIM][MAT_DIM])
{
	for(int i = 0; i < MAT_DIM; i++) {
		  for(int j = 0; j < MAT_DIM; j++) {
           //creation matrice diagonale superieur
            if ( j <= i ){
		         a[i][j] = ((float)(i + j) / (2 * MAT_DIM))+1;
            }else{
               a[i][j] = (float)0 ;
            }
            b[i][j] = (float)(i * j) / (MAT_DIM * MAT_DIM);
	   }
	}
}


//----------------------------------------------------
void gen_sw_matmult(
    mat_a_t a_inv[MAT_DIM][MAT_DIM],
    mat_b_t b_trans[MAT_DIM][MAT_DIM],
    result_t sw_res[MAT_DIM][MAT_DIM])
{
	// Generate the expected result
	// Iterate over the rows of the A matrix

	for(int i = 0; i < MAT_DIM; i++) {
	   // Iterate over the columns of the B matrix
		  for(int j = 0; j < MAT_DIM; j++) {
		  sw_res[i][j] = 0;
		  // Do the inner product of a row of A and col of B
		  for(int k = 0; k < MAT_DIM; k++) {
			 sw_res[i][j] += a_inv[i][k] * b_trans[k][j];
		  }
	   }
	}
}

//----------------------------------------------------
void check_matmult_result(
    result_t hw_res[MAT_DIM][MAT_DIM],
    result_t sw_res[MAT_DIM][MAT_DIM],
    int &err_cnt)
{
// Print result matrix
cout << "{" << endl;
for (int i = 0; i < MAT_DIM; i++) {
   cout << "{";
   for (int j = 0; j < MAT_DIM; j++) {
      cout << hw_res[i][j];
      // Check HW result against SW
      if (hw_res[i][j] != sw_res[i][j]) {
         err_cnt++;
         cout << "*";
      }
      if (j == MAT_DIM - 1)
         cout << "}" << endl;
      else
         cout << ",";
   }
}
cout << "}" << endl << endl;

cout << "Result by SW" << endl;
for (int i = 0; i < MAT_DIM; i++) {
   cout << "{";
   for (int j = 0; j < MAT_DIM; j++) {
      cout << sw_res[i][j];
      if (j == MAT_DIM - 1)
         cout << "}" << endl;
      else
         cout << ",";
   }
}
cout << "}" << endl;
}

//----------------------------------------------------
int main(int argc, char **argv)
{
	int err_cnt;

	err_cnt = test_matrix_mul();
    err_cnt = test_matrix_mul_core();

	return err_cnt;
}


