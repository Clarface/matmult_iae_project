void matrixinv(
	mat_a_t a[MAT_DIM][MAT_DIM],
	result_t res[MAT_DIM][MAT_DIM]){

    /*On considère res comme une matrice identité*/
    for (int i = 0; i < MAT_DIM; i++) {
        for (int j = 0; j < MAT_DIM; j++) {
            if (i == j) {
                res[i][j] = 1.0;
            } else {
                res[i][j] = 0.0;
            }
        }
    }

    /*On applique la méthode de Gauss-Jordan*/

    /*On calcule colone par colone*/
    int col;
    for (col = 0; col < MAT_DIM; col++) {

        /*Calcul du pivot*/
        MY_TYPE pivot = a[col][col];

        /*Diviser la ligne par le pivot dans les deux matrices*/
        int j;
        for (j = 0; j < MAT_DIM; j++) {
            a[col][j] = a[col][j] / pivot;
            res[col][j] = res[col][j] / pivot;
        }

        /* Soustraire des autres lignes pour obtenir des zéros en dessous et au-dessus du pivot */
        int i;
        for (i = 0; i < MAT_DIM; i++) {
            if (i != col) {
                MY_TYPE facteur = a[i][col];
                for (int j = 0; j < MAT_DIM; j++) {
                    a[i][j] = a[i][j] - facteur * a[col][j];
                    res[i][j] = res[i][j] - facteur * res[col][j];
                }
            }
        }
    }
}