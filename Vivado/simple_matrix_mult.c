//************************************************************
// Simple Matrix multiplication
//
// G. Sutter for teaching purpose.
// march 2014. For Vivado 2013.3
//
//************************************************************
#include <stdio.h>
#include "platform.h"
#include "xil_io.h"
#include "platform.h"
#include "xparameters.h"
#include "xtmrctr.h"
#include "simple_matrix_mult.h"

#include "xil_cache.h"
#include "xil_printf.h"

//#define PRINT_MED_TIME 1

// TIMER Instance
XTmrCtr timer_dev;
// a Matrix multiplicator instance
XMatrixmul_accel_core XMatrixmul0_dev;
//XMatrixmul_accel_core XMatrixmul1_dev;//11-10-21
XMatrixmul_accel_core_Config XMatrixmul0_config = { 0, XPAR_MATRIXMUL_ACCEL_CORE_0_S_AXI_CONTROL_BASEADDR };
//XMatrixmul_accel_core_Config XMatrixmul1_config = { 0, XPAR_MATRIXMUL_ACCEL_CORE_1_S_AXI_CONTROL_BASEADDR };//11-10-21

//---------------------------------------------------------------------------
void matrix_multiply_SW(float a[DIM][DIM], float b[DIM][DIM], float res[DIM][DIM])
{
  int ia, ib, id;
  float sum;

  // matrix multiplication of a A*B matrix
  for (ia = 0; ia < DIM; ++ia)
     for (ib = 0; ib < DIM; ++ib)
     { 	 sum = 0;
		 for (id = 0; id < DIM; ++id)
			 sum += a[ia][id] * b[id][ib];
		 res[ia][ib] = sum;
     }
}

float sign(float in){return (in < 0 ? -1 : 1);}

//---------------------------------------------------------------------------
void compute_matmult_mean_err(
	float sw_res[DIM][DIM],
	float hw_res[DIM * DIM],
    float *mean_err)
{
	float tmp;
	for (int i = 0; i < DIM; i++) {
	   for (int j = 0; j < DIM; j++) {
			tmp = sw_res[i][j] - hw_res[i * DIM + j];
			*mean_err += sign(tmp) * tmp;
	   }
	}
	*mean_err = *mean_err / (DIM * DIM);
}

//---------------------------------------------------------------------------
void print_accel_status(void)
{
	int isDone, isIdle, isReady;

	isDone = XMatrixmul_accel_core_IsDone(&XMatrixmul0_dev);
	isIdle = XMatrixmul_accel_core_IsIdle(&XMatrixmul0_dev);
	isReady = XMatrixmul_accel_core_IsReady(&XMatrixmul0_dev);
	xil_printf("MatMultAccel 0 Status: isDone %d, isIdle %d, isReady%d\r\n", isDone, isIdle, isReady);

////11-10-21
	//isDone = XMatrixmul_accel_core_IsDone(&XMatrixmul1_dev);
	//isIdle = XMatrixmul_accel_core_IsIdle(&XMatrixmul1_dev);
	//isReady = XMatrixmul_accel_core_IsReady(&XMatrixmul1_dev);
	//xil_printf("MatMultAccel 1 Status: isDone %d, isIdle %d, isReady%d\r\n", isDone, isIdle, isReady);
}
//---------------------------------------------------------------------------
int main(){
	int i, j;
	int status, err=0;
	float A[DIM][DIM], B[DIM][DIM];
	float res_sw[DIM][DIM];

	float A_hw0[2 * DIM * DIM];
	float *B_hw0 = A_hw0 + (DIM * DIM);
	float res_hw0[DIM * DIM];

	//float A_hw1[2 * DIM * DIM];
	//float *B_hw1 = A_hw1 + (DIM * DIM);
	//float res_hw1[DIM * DIM];


    float acc_factor;
	unsigned int input_size = SIZE * sizeof(float);
	unsigned int init_time, curr_time, calibration;
	unsigned int begin_time, end_time;
	unsigned int run_time_sw, run_time_hw = 0;
	#ifdef PRINT_MED_TIME
	unsigned int time_1, time_2, time_3, time_4, time_5, time_6;
	#endif

	init_platform();

	xil_printf("\r********************************\n\r");
	xil_printf("\r FP MATRIX MULT in Vivado HLS \n\n\r");

	// Setup timer
	status = XTmrCtr_Initialize(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	if(status != XST_SUCCESS){ print("\rError: timer setup failed\n"); }
	XTmrCtr_SetOptions(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID, XTC_ENABLE_ALL_OPTION);

	// Calibrate timer
	XTmrCtr_Reset(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	init_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	curr_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	calibration = curr_time - init_time;
	xil_printf("Calibrating the timer:\r\n");
	xil_printf("init_time: %d cycles.\r\n", init_time);
	xil_printf("curr_time: %d cycles.\r\n", curr_time);
	xil_printf("calibration: %d cycles.\r\n", calibration);

	XTmrCtr_Reset(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	begin_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	for (i = 0; i< 10000; i++);
	end_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	run_time_sw = end_time - begin_time - calibration;
	xil_printf("Loop time for 10000 iterations is %d cycles.\r\n", run_time_sw);

	xil_printf("\rBefore Start\r\n");

	// input data; Matrix Initiation
	for(i = 0; i < DIM; i++)
		for(j = 0; j < DIM; j++)
		{
			A[i][j] = (float)(i + j) / (2 * DIM);
			B[i][j] = (float)(i * j) / (DIM * DIM);

			A_hw0[i * DIM + j] = A[i][j];
			//A_hw1[i * DIM + j] = A[i][j];
			B_hw0[i * DIM + j] = B[i][j];
			//B_hw1[i * DIM + j] = B[i][j];

			res_sw[i][j] = 0;
			res_hw0[i * DIM + j] = 0;
			//res_hw1[i * DIM + j] = 0;
		}

	//call the software version of the function
	//print("\r now ARM is running the SW IP\n\r");

	XTmrCtr_Reset(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	begin_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);

	matrix_multiply_SW(A, B, res_sw);

	end_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	run_time_sw = end_time - begin_time - calibration;
	xil_printf("\r\nTotal run time for SW on ARM Processor (bare metal) is %d cycles.\r\n", run_time_sw);

	// Init the HW Accelerator 0;
	status = XMatrixmul_accel_core_CfgInitialize(&XMatrixmul0_dev, &XMatrixmul0_config);
	if(status != XST_SUCCESS){
		xil_printf("Error: example setup failed\r\n");
		return XST_FAILURE;
	}
	// Init the HW Accelerator 1;
	//status = XMatrixmul_accel_core_CfgInitialize(&XMatrixmul1_dev, &XMatrixmul1_config);
	//if(status != XST_SUCCESS){
	//	xil_printf("Error: example setup failed\r\n");
	//	return XST_FAILURE;
	//}

    // the interruption are not connected in fact.
	XMatrixmul_accel_core_InterruptGlobalDisable(&XMatrixmul0_dev);
	XMatrixmul_accel_core_InterruptDisable(&XMatrixmul0_dev, 1);
	//XMatrixmul_accel_core_InterruptGlobalDisable(&XMatrixmul1_dev);
	//XMatrixmul_accel_core_InterruptDisable(&XMatrixmul1_dev, 1);

    print_accel_status();

    XMatrixmul_accel_core_Set_input_r_r(&XMatrixmul0_dev, (u32)A_hw0);
	XMatrixmul_accel_core_Set_output_r_r(&XMatrixmul0_dev, (u32)res_hw0);
	//XMatrixmul_accel_core_Set_input_V(&XMatrixmul1_dev, (u32)A_hw1);
	//XMatrixmul_accel_core_Set_output_V(&XMatrixmul1_dev, (u32)res_hw1);

	// Flush caches to make the data visible from the hardware
	Xil_DCacheFlushRange((unsigned int)A_hw0, input_size * 2);
	//Xil_DCacheFlushRange((unsigned int)A_hw1, input_size * 2);

	print("\rCache cleared\n\r");

	xil_printf("\rSetup HW accelerator done\r\n");

	XTmrCtr_Reset(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	begin_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);

    //start the accelerator
	XMatrixmul_accel_core_Start(&XMatrixmul0_dev);
	//XMatrixmul_accel_core_Start(&XMatrixmul1_dev);

	while(!XMatrixmul_accel_core_IsDone(&XMatrixmul0_dev));
	//while(!XMatrixmul_accel_core_IsDone(&XMatrixmul1_dev));

	end_time = XTmrCtr_GetValue(&timer_dev, XPAR_AXI_TIMER_0_DEVICE_ID);
	run_time_hw = end_time - begin_time - calibration;
	xil_printf("Total run time for HW accelerator is %d cycles.\r\n",	run_time_hw);

	print_accel_status();

	Xil_DCacheInvalidateRange((unsigned int)res_hw0, input_size);
	//Xil_DCacheInvalidateRange((unsigned int)res_hw1, input_size);

	float mean_err = 0;

	compute_matmult_mean_err(res_sw, res_hw0, &mean_err);

	printf("\rMean error for accel 0 = %f\n\r", mean_err);

	//compute_matmult_mean_err(res_sw, res_hw1, &mean_err);

	//printf("\rMean error for accel 1 = %f\n\r", mean_err);

	// HW vs. SW speedup factor
	acc_factor = (float) run_time_sw / (float) run_time_hw;
	xil_printf("\r\033[1mAcceleration factor: %d.%d \033[0m\r\n\r\n",
			(int) acc_factor, (int) (acc_factor * 1000) % 1000);

    #ifdef PRINT_MED_TIME
	xil_printf("Time waiting to send 1st matrix: %d cycles\r\n", time_2 - time_1);
	//xil_printf("Time waiting to send 2nd matrix: %d cycles\r\n", time_4 - time_3);
	xil_printf("Time waiting to receive results: %d cycles\r\n", time_6 - time_5);
	#endif
	cleanup_platform();

	return  err;
}
