# project_matmult_IAE


## Objectif du projet

## A faire

### Ecriture des fonctions pour HLS

- Ajouter la fonction d'inversion (qui sera affectuée sur A) à [matrixmul.cpp](HLS\matrixmul.cpp) nommée *matrixinv(...)*
- Ajouter la fonction de transposition (qui sera affectuée sur B) à [matrixmul.cpp](HLS\matrixmul.cpp) nommée *matrixtrans(...)*
- Modifier la fonction *gen_matrix_ab* dans [matrixmul_test.cpp](HLS\matrixmul_test.cpp) de manière à ce que la matrice A soit triangulaire. 
- Mofifier la fonction *matrixmul_accel_core* dans [matrixmul.cpp](HLS\matrixmul.cpp) de sorte à ajouter nos deux nouvelles opération avant d'effectuer la multiplication de matrices.
- Modifier la fonction *gen_sw_matmult* dans [matrixmul_test.cpp](HLS\matrixmul_test.cpp) en ajoutant nos deux opérations au résultat de manière à ce que l'on puisse vérifier que nos nouveaux calculs sont corrects.

### 

## Notes d'Arthur
 - A mettre dans notre rapport:
 - - Questionnement vis-à-vis du passage en paramètre de la matrice,est-ce que la carte aurait pu accepter le passage des matrices en pointeur ?
 - - Reponse , le passage en argument est implicitement une reference donc j'ai pas eu de problème.