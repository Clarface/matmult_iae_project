//*****************************************************************************
// Matrix multiplication example
// Zynq coprocessor
// by G.Sutter for teaching purposes
// Revision History:
//  - march 2014, for Vivado HLS 2013.3
//
//*****************************************************************************

#include "matrixmul.h"
#include <utils/x_hls_utils.h>

using namespace hls;


void read_inputs(
	UINT_VALUE input[2 *MAT_DIM * MAT_DIM],
	mat_a_t mat_a[MAT_DIM][MAT_DIM],
	mat_b_t mat_b[MAT_DIM][MAT_DIM])
{
	// stream in first matrix
	read_a1: for(int i=0; i< MAT_DIM; i++){
		read_a2: for(int j=0; j< MAT_DIM; j++){
			//#pragma HLS PIPELINE
			union {	unsigned int ival; float oval; } converter;
			converter.ival = input[i * MAT_DIM + j];
			mat_a[i][j] = converter.oval;
			//mat_a[i][j] = aValue.data;
		}
	}

	// stream in second matrix
	read_b1: for(int i=0; i< MAT_DIM; i++){
		read_b2: for(int j=0; j< MAT_DIM; j++){
			//#pragma HLS PIPELINE
			union {	unsigned int ival; float oval; } converter;
			converter.ival = input[MAT_DIM * MAT_DIM + i * MAT_DIM + j];
			mat_b[i][j] = converter.oval;
			//mat_b[i][j] = aValue.data;
		}
	}
}

void write_outputs(
	result_t mat_res[MAT_DIM][MAT_DIM],
	UINT_VALUE output[MAT_DIM * MAT_DIM])
{
	// stream out result matrix
	write_res1: for(int i = 0; i < MAT_DIM; i++){
		write_res2: for(int j = 0; j < MAT_DIM; j++){
			//#pragma HLS PIPELINE
			union {	unsigned int oval; float ival; } converter;
			converter.ival = mat_res[i][j];
			output[i * MAT_DIM + j] = converter.oval;
		}
	}
}

void matrixinv(
        mat_a_t a[MAT_DIM][MAT_DIM],
        mat_a_t res[MAT_DIM][MAT_DIM]){

    // Initialisez la matrice inverse comme une matrice identit�
    matrixinv_label2:for (int i = 0; i < MAT_DIM; ++i) {
        matrixinv_label8:for (int j = 0; j < MAT_DIM; ++j) {
            res[i][j] = (i == j) ? 1 : 0;
        }
    }

        matrixinv_label3:for (int i = 0; i < MAT_DIM; ++i) {
        // Assurez-vous que la diagonale principale n'est pas nulle (sinon, vous ne pourrez pas inverser la matrice)
        if (a[i][i] == 0.0) {
            //fprintf(stderr, "La diagonale principale contient des z�ros. Impossible d'inverser la matrice.\n");
            return;
        }

        // �chelonnez la ligne i de la matrice d'origine et appliquez les m�mes op�rations � la matrice inverse
        mat_a_t scale = (MY_TYPE)1.0 / a[i][i];
        matrixinv_label7:for (int j = 0; j < MAT_DIM; ++j) {
            a[i][j] *= scale;
            res[i][j] *= scale;
        }

        // �liminez les �l�ments au-dessus et en dessous de la diagonale principale
        matrixinv_label4:for (int k = 0; k < MAT_DIM; ++k) {
            if (k != i) {
                mat_a_t factor = a[k][i];
                matrixinv_label6:for (int j = 0; j < MAT_DIM; ++j) {
                    a[k][j] -= factor * a[i][j];
                    res[k][j] -= factor * res[i][j];
                }
            }
        }
    }
}

void matrixtrans(	mat_b_t b[MAT_DIM][MAT_DIM],
					result_t res[MAT_DIM][MAT_DIM]
	){
	//Transposée de la matrice
	matrixtrans_label1:for(int i=0;i<MAT_DIM;i++){
		matrixtrans_label5:for(int j=0;j<MAT_DIM;j++){
		res[j][i]	=b[i][j];
		}
	}
}

void matrixmul(
	mat_a_t a[MAT_DIM][MAT_DIM],
	mat_b_t b[MAT_DIM][MAT_DIM],
	result_t res[MAT_DIM][MAT_DIM])
{
// partition with half dimension since BRAM has two ports
int const FACTOR = MAT_DIM/2;
//#pragma HLS INLINE off
//#pragma HLS array_partition variable=a block factor=FACTOR dim=2
//#pragma HLS array_partition variable=b block factor=FACTOR dim=1

  result_t accum;
  // Iterate over the rows of the A matrix
  Row: for(int i = 0; i < MAT_DIM; i++) {
    // Iterate over the columns of the B matrix

    Col: for(int j = 0; j < MAT_DIM; j++) {
      // Do the inner product of a row of A and col of B
//#pragma HLS PIPELINE II=1
      accum = 0;
      Prod: for(int k = 0; k < MAT_DIM; k++) {
        accum += reg(a[i][k] * b[k][j]);
        res[i][j] = accum; //if (k == (MAT_B_ROWS-1)) res[i][j] = accum;
      }

    }
  }
}

// --------------------------------------------------------
// main accelerator function, interfaces with AXI-S channels
void matrixmul_accel_core (
	UINT_VALUE input[2 *MAT_DIM * MAT_DIM],
	UINT_VALUE output[MAT_DIM * MAT_DIM])
{
#pragma HLS INTERFACE m_axi offset=slave port=input bundle=INPUT
#pragma HLS INTERFACE m_axi offset=slave port=output bundle=OUTPUT
#pragma HLS INTERFACE s_axilite port=input bundle=control
#pragma HLS INTERFACE s_axilite port=output bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

	mat_a_t mat_a[MAT_DIM][MAT_DIM];
	mat_a_t mat_a_inv[MAT_DIM][MAT_DIM];

	mat_b_t mat_b[MAT_DIM][MAT_DIM];
	mat_b_t mat_b_trans[MAT_DIM][MAT_DIM];

    result_t mat_res[MAT_DIM][MAT_DIM];
    int i, j;

    read_inputs(input, mat_a, mat_b);
	//Inversion de la matrice A
	//matrixinv(mat_a, mat_a_inv);

	//Transposition de la matrice B
	//matrixtrans (mat_b, mat_b_trans);

	// do Matrix multiplication
	matrixmul(mat_a, mat_b, mat_res);

	write_outputs(mat_res, output);
}





